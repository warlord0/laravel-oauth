<?php
/**
 * Social Account Model
 *
 * PHP Version 7.1
 *
 * @category Model
 * @package  App
 * @author   Paul Bargewell <paul.bargewell@charnwood.gov.uk>
 * @author   Paul Bargewell <paulb@whalesanctuary.co.uk>
 * @license  GPLv3.0 GNU Public License v3.0
 * @link     http://www.charnwood.gov.uk
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Social Account Model
 *
 * @category Model
 * @package  App
 * @author   Paul Bargewell <paul.bargewell@charnwood.gov.uk>
 * @author   Paul Bargewell <paulb@whalesanctuary.co.uk>
 * @license  GPLv3.0 GNU Public License v3.0
 * @link     http://www.charnwood.gov.uk
 */
class SocialAccount extends Model
{
    protected $fillable = ['provider_name', 'provider_id'];
    
    /**
     * Relationship to User
     *
     * @return BelongsTo
     */
    public function user() {
        return $this->belongsTo('App\User');
    }
}
