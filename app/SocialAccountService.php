<?php
/**
 * Social Account Service
 *
 * PHP Version 7.1
 *
 * @category Service
 * @package  Auth
 * @author   Paul Bargewell <paul.bargewell@charnwood.gov.uk>
 * @author   Paul Bargewell <paulb@whalesanctuary.co.uk>
 * @license  GPLv3.0 GNU Public License v3.0
 * @link     http://www.charnwood.gov.uk
 */
namespace App;

use Laravel\Socialite\Contracts\User as ProviderUser;

/**
 * Social Account Service
 *
 * @category Service
 * @package  Auth
 * @author   Paul Bargewell <paul.bargewell@charnwood.gov.uk>
 * @author   Paul Bargewell <paulb@whalesanctuary.co.uk>
 * @license  GPLv3.0 GNU Public License v3.0
 * @link     http://www.charnwood.gov.uk
 */
class SocialAccountService
{
    /**
     * Find or create a social account
     *
     * @param ProviderUser $providerUser OAuth2 user
     * @param String       $provider     OAuth provider
     * 
     * @return App\User
     */
    public function findOrCreate(ProviderUser $providerUser, $provider)
    {
        $account = SocialAccount::where('provider_name', $provider)
            ->where('provider_id', $providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $user = User::where('email', $providerUser->getEmail())->first();

            if (! $user) {
                $user = User::create(
                    [  
                        'email' => $providerUser->getEmail(),
                        'name'  => $providerUser->getName(),
                    ]
                );
            }

            $user->accounts()
                ->create(
                    [
                        'provider_id'   => $providerUser->getId(),
                        'provider_name' => $provider,
                    ]
                );

            return $user;

        }
    }
}