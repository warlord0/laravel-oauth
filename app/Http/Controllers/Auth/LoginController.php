<?php
/**
 * LoginController
 *
 * PHP Version 7.1
 *
 * @category Auth
 * @package  App
 * @author   Paul Bargewell <paul.bargewell@charnwood.gov.uk>
 * @author   Paul Bargewell <paulb@whalesanctuary.co.uk>
 * @license  GPLv3.0 GNU Public License v3.0
 * @link     http://www.charnwood.gov.uk
 */
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use App\SocialAccountService;

use Socialite;

/**
 * LoginController
 *
 * @category Auth
 * @package  App
 * @author   Paul Bargewell <paul.bargewell@charnwood.gov.uk>
 * @author   Paul Bargewell <paulb@whalesanctuary.co.uk>
 * @license  GPLv3.0 GNU Public License v3.0
 * @link     http://www.charnwood.gov.uk
 */
class LoginController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @param String $provider Name of the OAuth provider to use. eg. github, gitlab
     * 
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider(String $provider)
    {
        switch ($provider) {
        case 'gitlab':
            return Socialite::driver($provider)
                ->scopes(['read_user']) // We need to pass the scope for gitlab
                ->redirect();
            break;
        default:
            return Socialite::driver($provider)
                ->redirect();
            break;
        }
    }

    /**
     * Obtain the user information from GitHub.
     * 
     * @param SocialAccountService $accountService Account service
     * @param String               $provider       Name of the OAuth provider to use.
     *                                             eg. github, gitlab
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(
        SocialAccountService $accountService, String $provider
    ) {
        $user = Socialite::driver($provider)->user();

        $authUser = $accountService->findOrCreate(
            $user,
            $provider
        );

        auth()->login($authUser, true);

        return redirect()->to('/home');
    }

    /**
     * Logout
     *
     * @return Redirect
     */
    public function logout() {
        auth()->logout();
        return redirect()->to('/');
    }
}