<?php
/**
 * Alter the Users Table for Socialite
 *
 * PHP Version 7.1
 *
 * @category Migration
 * @package  Auth
 * @author   Paul Bargewell <paul.bargewell@charnwood.gov.uk>
 * @author   Paul Bargewell <paulb@whalesanctuary.co.uk>
 * @license  GPLv3.0 GNU Public License v3.0
 * @link     http://www.charnwood.gov.uk
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Alter the Users Table for Socialite
 * 
 * Sets the email and password fields as nullable
 *
 * @category Migration
 * @package  Auth
 * @author   Paul Bargewell <paul.bargewell@charnwood.gov.uk>
 * @author   Paul Bargewell <paulb@whalesanctuary.co.uk>
 * @license  GPLv3.0 GNU Public License v3.0
 * @link     http://www.charnwood.gov.uk
 */
class AlterUsersTableForSocialite extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->string('email')->nullable()->change();
                $table->string('password')->nullable()->change();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->string('email')->nullable(false)->change();
                $table->string('password')->nullable(false)->change();
            }
        );
    }
}
