<?php
/**
 * Social Accounts table linked to Users
 *
 * PHP Version 7.1
 *
 * @category Migration
 * @package  Auth
 * @author   Paul Bargewell <paul.bargewell@charnwood.gov.uk>
 * @author   Paul Bargewell <paulb@whalesanctuary.co.uk>
 * @license  GPLv3.0 GNU Public License v3.0
 * @link     http://www.charnwood.gov.uk
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Social Accounts table linked to Users
 *
 * @category Migration
 * @package  Auth
 * @author   Paul Bargewell <paul.bargewell@charnwood.gov.uk>
 * @author   Paul Bargewell <paulb@whalesanctuary.co.uk>
 * @license  GPLv3.0 GNU Public License v3.0
 * @link     http://www.charnwood.gov.uk
 */
class CreateSocialAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'social_accounts',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('user_id');
                $table->string('provider_name')->nullable();
                $table->string('provider_id')->unique()->nullable();
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_accounts');
    }
}
