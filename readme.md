# Laravel Socialite Authentication

Authenticate using OAuth providers Facebook, Google, Github and Gitlab.

## Functionality

Using Oath via a single login controller by passing in the provider to be used for authentication.

### .env Settings

Each OAuth provider requires `.env` settings which in turn are used by `services.php`.

eg.

```dotenv
GITHUB_CLIENT_ID=mysupersecretkey
GITHUB_SECRET_ID=mysupersecretkey
GITHUB_REDIRECT=http://localhost:8000/auth/provider
```

### services.php

```php
'github' => [
    'client_id' => env('GITHUB_CLIENT_ID'),
    'client_secret' => env('GITHUB_CLIENT_SECRET'),
    'redirect' => env('GITHUB_REDIRECT')
],
```

### Notes

The database in this dev project is a simple sqlite flat file located in `database/database.sqlite`.

It you need to recreate it and run migrations delete it and creste an empty file using:

```console
touch database/database.sqlite
```

The data stored in the tables does not include passwords as they form part of the OAuth authentication process.

Email addresses are also optional with some providers (eg. github) so a means of querying the user and storing them in the profile may be required.
